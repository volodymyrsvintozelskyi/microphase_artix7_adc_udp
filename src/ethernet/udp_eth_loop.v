// --------------------------------------------------------------------
// Copyright (c) 2022 by MicroPhase Technologies Inc. 
// --------------------------------------------------------------------
//
// Permission:
//
//   MicroPhase grants permission to use and modify this code for use
//   in synthesis for all MicroPhase Development Boards.
//   Other use of this code, including the selling 
//   ,duplication, or modification of any portion is strictly prohibited.
//
// Disclaimer:
//
//   This VHDL/Verilog or C/C++ source code is intended as a design reference
//   which illustrates how these types of functions can be implemented.
//   It is the user's responsibility to verify their design for
//   consistency and functionality through the use of formal
//   verification methods.  MicroPhase provides no warranty regarding the use 
//   or functionality of this code.
//
// --------------------------------------------------------------------
//           
//                     MicroPhase Technologies Inc
//                     Shanghai, China
//
//                     web: http://www.microphase.cn/   
//                     email: support@microphase.cn
//
// --------------------------------------------------------------------
// --------------------------------------------------------------------
//   
// Major Functions:   
//
// --------------------------------------------------------------------
// --------------------------------------------------------------------
//
//  Revision History    
//  Date          By            Revision    Change Description
//---------------------------------------------------------------------
// 2022.07.19     Ao Guohua     1.0          Original
//                                                      
// --------------------------------------------------------------------
// --------------------------------------------------------------------
module udp_eth_loop #(
     parameter BOARD_MAC_ADDR = 48'h00_11_22_33_44_55 ,       //目的的MAC地址
     parameter BOARD_IP_ADDR = {8'd192, 8'd168, 8'd0, 8'd4}  //FPGA IP地址)
   )
   (    
        input                clk          ,
        input                rst_n        , //复位信号
        //rgmii接受接口
        input                rgmii_rxc    ,
        input                rgmii_rx_ctl ,
        input       [3:0]    rgmii_rd     ,
        //rgmii发送接口
        output               rgmii_txc   ,
        output               rgmii_tx_ctl,
        output       [3:0]   rgmii_td    ,
        //以太网复位信号        
        output               phy_rst_n 

    );
wire         gmii_rxc    ;  
wire         gmii_rx_dv  ;
wire         gmii_rx_err ;
wire [7:0]   gmii_rd     ;
wire [128:0] status      ;
wire [7:0]   fil_data    ;
wire         po_data_vld ;
wire [7:0]   po_data     ;
wire         clk_200m    ;

assign phy_rst_n=1'b1; //让以太网芯片处于工作状态
delay_clock delay_clock
   (
    // Clock out ports
    .clk_out1(clk_200m),     // output clk_out1
    // Status and control signals
    .resetn(rst_n), // input resetn
    .locked(),       // output locked
   // Clock in ports
    .clk_in1(clk));      // input clk_in1
rgmii_to_gmii rgmii_to_gmii(
    .idelay_clk  (clk_200m),
    .rst         (!rst_n), //复位信号
    //rgmii 接口
    .rgmii_rxc   (rgmii_rxc),
    .rgmii_rx_ctl(rgmii_rx_ctl),
    .rgmii_rd    (rgmii_rd    ),
    //gmii  接口   
    .gmii_rxc    (gmii_rxc    ),
    .gmii_rx_dv  (gmii_rx_dv  ),
    .gmii_rx_err (),
    .gmii_rd     (gmii_rd     )
    );
udp_eth_rx  #(
          .BOARD_MAC_ADDR (BOARD_MAC_ADDR ) ,       //目的的MAC地址
          .BOARD_IP_ADDR  (BOARD_IP_ADDR )           //FPGA IP地址
   ) udp_eth_rx
   (
          .clk         (gmii_rxc    ),
          .rst         (!rst_n      ),
          .gmii_rx_dv  (gmii_rx_dv     ),
          .gmii_rd     (gmii_rd     ), 
          .status      (status      ),
          .fil_data    (fil_data    ),
          .fil_data_vld(fil_data_vld)
    );
udp_eth_tx #(
          .BOARD_MAC_ADDR (BOARD_MAC_ADDR ) ,       //目的的MAC地址
          .BOARD_IP_ADDR  (BOARD_IP_ADDR )           //FPGA IP地址
)udp_eth_tx 
(
     .clk         (gmii_rxc),
     .rst         (!rst_n),
     .status      (status),
     .fil_data_vld(fil_data_vld),
     .fil_data    (fil_data),
     .po_data_vld (po_data_vld),
     .po_data     (po_data) 
    );
gmii_to_rgmii  gmii_to_rgmii(
    .rst         (!rst_n), //复位信号
    //gmii  接口  去掉了gmii_error   
    .gmii_txc    (gmii_rxc),
    .gmii_tx_dv  (po_data_vld),
    .gmii_tx_err (1'b0),  //0表示没有错误
    .gmii_td     (po_data),
     //rgmii 接口
    .rgmii_txc    (rgmii_txc   ),
    .rgmii_tx_ctl (rgmii_tx_ctl),
    .rgmii_td     (rgmii_td    )
    );
ila_udp_eth_loop ila_udp_eth_loop (
	.clk(gmii_rxc), // input wire clk

	.probe0(gmii_rx_dv), // input wire [0:0]  probe0  
	.probe1(gmii_rd), // input wire [7:0]  probe1 
	.probe2(fil_data_vld), // input wire [0:0]  probe2 
	.probe3(fil_data), // input wire [7:0]  probe3 
	.probe4(po_data_vld), // input wire [0:0]  probe4 
	.probe5(po_data) // input wire [7:0]  probe5
);
endmodule
