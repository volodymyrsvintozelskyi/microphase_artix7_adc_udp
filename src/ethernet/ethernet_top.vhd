library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

entity ethernet_top is
  generic (
    MAC_ADDRESS : std_logic_vector(47 downto 0) := x"00_0A_35_00_01_02";
    IP_ADDR : std_logic_vector(31 downto 0) := x"C0A80004"
  );
  port (
    clk : in std_logic;
    rst_n : in std_logic;

    -- Input serial interface
    data_i       : in std_logic_vector(15 downto 0);
    data_valid_i : in std_logic;
    data_ack     : out std_logic := '0';

    -- Ethernet (PHY) signals
    rgmii_txd       : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rgmii_tx_ctl    : OUT STD_LOGIC;
    rgmii_txc       : OUT STD_LOGIC
  );
end entity;

architecture rtl of ethernet_top is
  
  signal gmii_rxc: std_logic;
  signal gmii_rx_dv: std_logic;
  signal gmii_rx_err: std_logic := '0';
  signal gmii_rd: std_logic_vector(7 downto 0);
  
  signal frame: std_logic_vector(15 downto 0);
  signal data: std_logic_vector(7 downto 0);
  signal dat_val: std_logic;

  signal status: std_logic_vector(128 downto 0);
  alias pkg_len : std_logic_vector(15 downto 0) is status(127 downto 112);
  alias ip_len : std_logic_vector(15 downto 0) is status(111 downto 96);
  alias udp_len : std_logic_vector(15 downto 0) is status(95 downto 80);
  alias pc_mac_addr : std_logic_vector(47 downto 0) is status(79 downto 32);
  alias pc_ip_addr : std_logic_vector(31 downto 0) is status(31 downto 0);

  type fsm_t is (IDLE, WAITING, CONSTRUCT_1, CONSTRUCT_2);
  signal fsm: fsm_t;
  signal cnt: integer range 0 to 63;

  component gmii_to_rgmii is
    port (
      rst         : in std_logic; --//复位信号
      idelay_clk  : in std_logic;
      -- gmii
      gmii_txc    : in std_logic;
      gmii_tx_dv  : in std_logic;
      gmii_tx_err : in std_logic;
      gmii_td     : in std_logic_vector(7 downto 0);
      -- rgmii
      rgmii_txc     : out std_logic;
      rgmii_tx_ctl  : out std_logic;
      rgmii_td      : out std_logic_vector(3 downto 0)
    ); end component;

  component udp_eth_tx is
    generic (
      BOARD_MAC_ADDR : std_logic_vector(47 downto 0) := x"00_11_22_33_44_55";
      BOARD_IP_ADDR  : std_logic_vector(31 downto 0) := x"C0A80004"
    );
    port (
      clk         : in std_logic;
      rst         : in std_logic;
      status      : in std_logic_vector(128 downto 0);
      fil_data_vld: in std_logic;
      fil_data    : in std_logic_vector(7 downto 0);
      po_data_vld : out std_logic;
      po_data     : out std_logic_vector(7 downto 0)
    );
  end component;

begin

  gmii_rxc <= clk;
  pc_mac_addr <= (others => '1');
  pc_ip_addr <= (others => '1');
  udp_len <= std_logic_vector(to_unsigned( 2 + 8 , 16));
  ip_len <= std_logic_vector(to_unsigned( 20 + 10 , 16));
  pkg_len <= std_logic_vector(to_unsigned( 22 + 20 + 10 , 16));

  arbiter_p: process (clk) is
  begin
    if rising_edge(clk) then
      data_ack <= '0';
      dat_val <= '0';
      
      if rst_n = '0' then
        fsm <= IDLE;
      else
        case fsm is
          when IDLE =>
            if data_valid_i = '1' then
              frame <= data_i;
              data_ack <= '1';
              fsm <= WAITING;
              cnt <= 50;
            end if;
          when WAITING =>
            dat_val <= '1';
            if cnt = 1 then
              fsm <= CONSTRUCT_1;
            else
              cnt <= cnt - 1;
            end if;
          when CONSTRUCT_1 =>
            data <= frame(15 downto 8);
            dat_val <= '1';
            fsm <= CONSTRUCT_2;
          when CONSTRUCT_2 =>
            data <= frame(7 downto 0);
            dat_val <= '1';
            fsm <= IDLE;
          when others =>
            fsm <= IDLE;
        end case;
      end if;
    end if;
  end process;

  gmii_to_rgmii_inst : gmii_to_rgmii
    port map (
      rst => not rst_n,
      idelay_clk => clk,
      gmii_txc => gmii_rxc,
      gmii_tx_dv => gmii_rx_dv,
      gmii_tx_err => gmii_rx_err,
      gmii_td => gmii_rd,
      rgmii_txc => rgmii_txc,
      rgmii_tx_ctl => rgmii_tx_ctl,
      rgmii_td => rgmii_txd
    );
    
  udp_eth_tx_inst : udp_eth_tx
    generic map (
      BOARD_MAC_ADDR => MAC_ADDRESS,
      BOARD_IP_ADDR => IP_ADDR
    )
    port map (
      clk => clk,
      rst => not rst_n,
      status => status,
      fil_data_vld => dat_val,
      fil_data => data,
      po_data_vld => gmii_rx_dv,
      po_data => gmii_rd
    );

end architecture;
