// --------------------------------------------------------------------
// Copyright (c) 2022 by MicroPhase Technologies Inc. 
// --------------------------------------------------------------------
//
// Permission:
//
//   MicroPhase grants permission to use and modify this code for use
//   in synthesis for all MicroPhase Development Boards.
//   Other use of this code, including the selling 
//   ,duplication, or modification of any portion is strictly prohibited.
//
// Disclaimer:
//
//   This VHDL/Verilog or C/C++ source code is intended as a design reference
//   which illustrates how these types of functions can be implemented.
//   It is the user's responsibility to verify their design for
//   consistency and functionality through the use of formal
//   verification methods.  MicroPhase provides no warranty regarding the use 
//   or functionality of this code.
//
// --------------------------------------------------------------------
//           
//                     MicroPhase Technologies Inc
//                     Shanghai, China
//
//                     web: http://www.microphase.cn/   
//                     email: support@microphase.cn
//
// --------------------------------------------------------------------
// --------------------------------------------------------------------
//   
// Major Functions:   
//
// --------------------------------------------------------------------
// --------------------------------------------------------------------
//
//  Revision History    
//  Date          By            Revision    Change Description
//---------------------------------------------------------------------
// 2022.07.19     Ao Guohua     1.0          Original
//                                                      
// --------------------------------------------------------------------
// --------------------------------------------------------------------
//此模块用于接收以太网数据并且过滤掉IP地址和MAC地址不对的数据
module udp_eth_rx
   #(parameter BOARD_MAC_ADDR = 48'h00_11_22_33_44_55 ,       //目的的MAC地址
     parameter BOARD_IP_ADDR = {8'd192, 8'd168, 8'd0, 8'd4}  //FPGA IP地址)
   )
   (
          input          clk         ,
          input          rst         ,
          input          gmii_rx_dv  ,
          input  [7:0]   gmii_rd     , 
          output [128:0] status      ,
          output [7:0]   fil_data    ,
          output         fil_data_vld
    );
wire        crc_last;
wire        crc_err ;
wire        buf_busy;
wire        rd_ack  ;
wire        rd_req  ;
eth_rx_crc udp_eth_rx_crc(
        .clk         (clk     ),
        .rst         (rst     ),
        .gmii_rx_dv  (gmii_rx_dv ),
        .gmii_rd     (gmii_rd ),
        .crc_last    (crc_last),//输出一个高电平表示校验结束
        .crc_err     (crc_err ) //用于判断校验是否正确
    );
udp_eth_rx_info  #(  
        .BOARD_MAC_ADDR (BOARD_MAC_ADDR), //目的的MAC地址
        .BOARD_IP_ADDR (BOARD_IP_ADDR)  //FPGA IP地址
)  udp_eth_rx_info
  (
        .rst       (rst       ),
        .clk       (clk       ),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rd   (gmii_rd   ),
        .crc_err   (crc_err   ),//crc校验是否有错
        .crc_last  (crc_last  ),//校验结束同时也表明一帧结束
        .buf_busy  (buf_busy  ),//存储数据的模块是否忙碌
        .status    (status    ),//用于寄存一帧数据的长度以及是否错误
        .rd_ack    (rd_ack    ),//data_buferr对rd_req的应答信号
        .rd_req    (rd_req    ) //向data_buffer发出读出数据的要求
    );
udp_eth_rx_data udp_eth_rx_data(
        .clk         (clk         ),
        .rst         (rst         ),
        .gmii_rx_dv  (gmii_rx_dv  ),
        .gmii_rd     (gmii_rd     ),
        .rd_req      (rd_req      ),
        .rd_ack      (rd_ack      ),
        .status      (status      ),
        .buf_busy    (buf_busy    ),
        .fil_data    (fil_data    ),
        .fil_data_vld(fil_data_vld)
    );
endmodule
