library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

entity tb_top is
end entity;

architecture rtl of tb_top is

    signal clk: std_logic := '0';
    signal rst_n: std_logic := '0';
    signal rgmii_txd: std_logic_vector(3 downto 0);
    signal rgmii_tx_ctl: std_logic;
    signal rgmii_txc: std_logic;
    signal rgmii_rxd: std_logic_vector(3 downto 0);
    signal rgmii_rx_ctl: std_logic;
    signal rgmii_rxc: std_logic;
    signal phy_rst_n: std_logic;

    signal adc_data      : std_logic_vector(15 DOWNTO 0);
    signal adc_otr       : std_logic;  
    signal adc_clock     : std_logic;

    constant half_period: time := 10 ns;

begin

    clk <= not clk after half_period;
    rst_n <= '1' after 1 ns, '0' after 10 ns, '1' after 100 ns;

    top_inst: entity work.top
    port map (
      clk          => clk,
      rst_n        => rst_n,

      adc_data     => adc_data,
      adc_otr      => adc_otr,
      adc_clock    => adc_clock,

      rgmii_txd    => rgmii_txd,
      rgmii_tx_ctl => rgmii_tx_ctl,
      rgmii_txc    => rgmii_txc,
      rgmii_rxd    => rgmii_rxd,
      rgmii_rx_ctl => rgmii_rx_ctl,
      rgmii_rxc    => rgmii_rxc,
      phy_rst_n    => phy_rst_n
    );

end architecture;
