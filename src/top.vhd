library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

entity top is
  generic (
    CLK_DIV : integer := 50000
  );
  port (
    clk           : in std_logic;
    rst_n         : in std_logic;

    -- ADC in
    adc_data      : in std_logic_vector(15 DOWNTO 0);
    adc_otr       : in std_logic;  
    adc_clock     : out std_logic;

    -- Ethernet (PHY) signals
    rgmii_txd       : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rgmii_tx_ctl    : OUT STD_LOGIC;
    rgmii_txc       : OUT STD_LOGIC;
    rgmii_rxd       : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rgmii_rx_ctl    : IN STD_LOGIC;
    rgmii_rxc       : IN STD_LOGIC;

    phy_rst_n       : OUT STD_LOGIC := '1' 
  );
end entity;

architecture rtl of top is

  signal serial_data: std_logic_vector(15 downto 0);
  signal serial_data_valid: std_logic;
  signal serial_data_ack: std_logic;

  signal cnt: integer range 0 to CLK_DIV - 1 := 0;
begin

  adc_clock_gen: process (clk) is
  begin
    if rising_edge(clk) then
      if rst_n = '0' then
        cnt <= 0;
        adc_clock <= '0';
        serial_data_valid <= '0';
      else
        if serial_data_ack = '1' then
          serial_data_valid <= '0';
        end if;

        if cnt = CLK_DIV - 1 then
          cnt <= 0;
          adc_clock <= not adc_clock;
          if adc_clock = '1' then
            serial_data <= adc_data;
            serial_data_valid <= '1';
          end if;
        else
          cnt <= cnt + 1;
        end if;
      end if;
    end if;
  end process;

  ethernet_top_inst: entity work.ethernet_top
  port map (
    clk                  => clk,
    rst_n                => rst_n,
    data_i               => serial_data,
    data_valid_i         => serial_data_valid,
    data_ack             => serial_data_ack,
    rgmii_txd            => rgmii_txd,
    rgmii_tx_ctl         => rgmii_tx_ctl,
    rgmii_txc            => rgmii_txc
  );

end architecture;
