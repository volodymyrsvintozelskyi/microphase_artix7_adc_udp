# Synthesis 

```
vivado -mode tcl -source microphase_artix7_adc_udp.tcl
vivado -mode tcl -source synth_with_vivado.tcl
```

Bitstream will be in `vivado_project/microphase_artix7_adc_udp.runs/impl_1/`

# Uploading

Use vivado GUI

# Issues?

Check Constraints.xdc file for port mapping. Make sure that the port mapping is ok
